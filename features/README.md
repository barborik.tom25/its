Testování a dynamická analýza | Projekt 1 - Testovací scénáře (xbarbo06)
========================================================================

Testovaná část
--------------

Testuje se objednávání zboží. To zahrnuje kroky: 
1. Stránka košíku 
2. Výběr způsobu objednávání (přihlášení / nepřihlášený / nepřihlášený - registrace) 
3. Doručovací adresa 
4. Platební metoda 
5. Potvrzení objednávky

1. Stránka košíku
-----------------

Zde se testuje manipulace se zbožím v košíku. To zahrnuje změnu množství
a odebírání položek a vliv této akce na cenu košíku. Používání poukazů a dárkových voucherů.

2. Objednávání - 1. krok
------------------------

Na tomto kroku se testuje přihlašování existujícího / neexistujícího
uživatele do obchodu.

3. Objednávání - 2. krok
------------------------

Tady se testování dělí do tří featurů:
1. se zaměřuje na hosta, tj. na zákazníka, který provádí jednorázový nákup a nechce se registrovat. 
2. se zaměřuje na hosta, který se při objednávání chce i zaregistrovat. 
3. se zaměřuje na přihlášeného uživatele, který buď má již předdefinované adresy na svém účtu nebo si teprve první vytvoří při objednávání.

Všechny featury testují převážně funkčnost formuláře, jeho reakce na
špatně vyplněné nebo vůbec nevyplněné pole.

4. Objednávání - 3. krok
------------------------

Na tomto kroku se testuje, zda se systém chová podle specifikace, při
neodsouhlasení podmínek nebo nevybrání platební metody.

5. Objednávání - 4. krok
------------------------

Při posledním kroku se kontroluje, jestli systém uložil objednávku
správně do systému.
