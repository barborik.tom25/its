Feature: Zákazník je spokojený s obsahem košíku a chce si ho jít objednat.
	Protože ví, že se do obchodu bude častěji vracet, rovnou si chce při
	objednávání vytvořit účet.
	(Objednávání - 2. krok (osobní údaje, adresa) - registrace)

	Scenario Outline: Zákazník vyplnil formulář, ale přehlédl povinný údaj.
		Given zakaznik se  nachazi na registraci
		When zakaznik nevyplni povinny udaj <required>
		Then zakaznik je upozornen
		
	 Examples: Povinne udaje
   	  | required                   |  
	  | firstname                  |
      | lastname                   |
      | email                      |
	  | telephone                  |
      | password                   |
      | address_1                  |
      | city                       |
      | postcode                   |
	  | country_id                 |
	  | zone_id	                   |


	Scenario Outline: Zákazník vyplnil všechny povinné údaje ve formuláři,
		ale u některého pole omylem nenapsal všechny znaky.
		Given zakaznik se nachazi na registraci
		When zakaznik vyplni min jak <min> pro <required>
		Then zakaznik je upozornen
		
	 Examples: Povinne udaje - min
   	  | required                   | min    |
	  | firstname                  | 1      |
      | lastname                   | 1      |
	  | telephone                  | 3      |
      | password                   | 4      |
      | address_1                  | 3      |
      | city                       | 2      |
      | postcode                   | 2      |

	Scenario Outline: Zákazník vyplnil všechny povinné prvky formuláře,
		ale u některého pole se nedopatřením přepsal.
		Given zakaznik se nachazi na registraci
		When zakaznik vyplni vice jak <max> pro <required>
		Then zakaznik je upozornen
		
	 Examples: Povinne udaje - max
   	  | required                   | max    |
	  | firstname                  | 32     |
      | lastname                   | 32     |
	  | telephone                  | 32     |
      | password                   | 20     |
      | address_1                  | 128    |
      | city                       | 128    |
      | postcode                   | 10     |

	Scenario: Zákazník při vyplňování formuláře špatně vyplnil pole s e-mailem,
		čímž vzikla nevalidní adresa.
		Given zakaznik se nachazi na registraci
		When zakaznik zada spatny format emailu
		Then zakaznik je upozornen na jeho validitu

	Scenario: Zákazník vyplnil všechny potřebné pole formuláře správně a
		chtěl by pokračovat na další krok a tím se i zaregistrovat.
		Given zakaznik se nachazi na registraci
		When zakaznik zada spravne udaje
		Then zakaznik je registrovan

