Feature: Zákazník už prošel všechny kroky objednávání a je připraven
	objednávku potvrdit. (Objednávání - 4. krok (potvrzení objednávky))

	Scenario: Zákazník vyplnil všechny potřebné údaje, překontroloval, zda
		vše sedí a potvrdí objednávku.
		Given zakaznik se nachazi na potvrzeni objednavky
		When zakaznik potvrdi objednavku
		Then objednavka je vytvorena
