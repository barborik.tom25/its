Feature: Zákazník po spokojeném překontrolování obsahu svého košíku pokračoval
	na objednávání, při kterém se rozhodl, protože se zde už v minulosti
	registroval, že bude objednávat jako přihlášený uživatel. Po úspěšném přihlášení
	se tedy dostal na formulář s potřebnými údaji.
	(Objednávání - 2. krok (osobní údaje, adresa) - přihlášený uživatel)

	Scenario: Uživatel si vytvořil již předdefinované adresy pro doručení
		a tyto adresy se mu zobrazí v seznamu.
		Given uzivatel ma ulozenu alespon jednu dorucovaci adresu
		Then je mu nabidnuto si vybrat nekterou z ulozenych

	Scenario: Uživatel si nikdy neuložil ani jednu předdefinovanou adresu,
		a proto je mu zobrazen formulář, pro její vytvoření.
		Given uzivatel nema ulozenu ani jednu dorucovaci adresu
		Then uzivateli je zobrazen formular
		
	Scenario Outline: Uživatel vyplnil formulář, ale zapomněl vyplnit
		některé důležité pole.
		Given uzivatel se nachazi na formulari
		When uzivatel nevyplni povinny udaj <required>
		Then uzivatel je upozornen
		
	 Examples: Povinne udaje
   	  | required                   |  
	  | firstname                  |
      | lastname                   |
      | address_1                  |
      | city                       |
      | postcode                   |
	  | country_id                 |
	  | zone_id	                   |
	  
	Scenario Outline: Uživatel vyplnil všechny prvky ve formuláři, ale v
		některém zapomněl na pár písmen, čímž nedodržel minimální délku.
		Given uzivatel se nachazi na formulari
		When uzivatel vyplni min jak <min> pro <required>
		Then uzivatel je upozornen
		
	 Examples: Povinne udaje - min
   	  | required                   | min    |
	  | firstname                  | 1      |
      | lastname                   | 1      |
      | address_1                  | 3      |
      | city                       | 2      |
      | postcode                   | 2      |

	Scenario Outline: Uživatel vyplnil všechny nutné pole formuláře, ale
		při psaní se několikrát na klávesnici překlinul a zapsal více znaků,
		než kolik je povoleno.
		Given uzivatel se nachazi na formulari
		When uzivatel vyplni vice jak <max> pro <required>
		Then uzivatel je upozornen
		
	 Examples: Povinne udaje - max
   	  | required                   | max    |
	  | firstname                  | 32     |
      | lastname                   | 32     |
      | address_1                  | 128    |
      | city                       | 128    |
      | postcode                   | 10     |

	Scenario: Uživatel vyplnil vše v řádném rozmezí délky, ale při psaní
		své e-mailové adresy se seknul a napsal ji špatně.
		Given uzivatel se nachazi na formulari
		When uzivatel zada spatny format emailu
		Then uzivatel je upozornen na jeho validitu

	Scenario: Uživatel vyplnil vše ve formuláři tak, jak měl, čímž
		dodržel všechny pravidla a chtěl by pokračovat na další krok.
		Given uzivatel se nachazi na formulari
		When uzivatel zada spravne udaje
		Then uzivatel je pusten na vyber platby
		
	Scenario: Uživatel si vybral jednu z jeho předdefinovaných adres
		a pokračuje na výběr platební metody.
		Given uzivateli je zobrazen vyber adres
		When uzival vybere jednu adresu
		Then uzivatel je pusten na vyber platby

