from behave import *
from control.cart import *
cc = None

@given('v kosiku se nachazi {count} - {id}')
def step_impl(context, count, id):
    global cc 
    cc = CartControl()
    cc.add_product(int(id), (count))
    pass

@given('administrator povoli vyrobek ({id})')
def step_impl(context, id):
    global cc 
    cc = CartControl()
    cc.enable_product(id)
    pass

@when('uzivatel odebere polozku {id}')
def step_impl(context, id):
    cc.remove_product(id)
    pass

@when('uzivatel zmeni mnozstvi {id} o {count}')
def step_impl(context, id, count):
    cc.change_amount(int(id), int(count))
    pass

@when('uzivatel pouzije kupon {coupon}')
def step_impl(context, coupon):
    cc.use_coupon(coupon)
    pass

@when('uzivatel pouzije voucher {voucher}')
def step_impl(context, voucher):
    cc.use_voucher(voucher)
    pass

@when('administrator zakaze vyrobek ({id})')
def step_impl(context, id):
    cc.disable_product(id)
    pass

@then('kosik je prazdny')
def step_impl(context):
    assert cc.cart_amount() == 0
    pass

@then('v kosiku se nachazi {count} polozka')
def step_impl(context, count):
    assert cc.cart_amount() == int(count)
    pass

@then('celkova cena je {value}')
def step_impl(context, value):
    assert cc.get_total_value().replace(',', '') == value
    pass