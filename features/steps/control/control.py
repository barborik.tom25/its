from .config import *;

import time
import re

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys

def wait_for(condition_function):
    start_time = time.time() 
    while time.time() < start_time + 3: 
        if condition_function():
            return True 
        else: 
            time.sleep(0.1) 
    raise Exception('Timeout waiting for {}'.format(condition_function.__name__) )

class wait_for_page_load(object):

  def __init__(self, browser):
    self.browser = browser
    
  def __enter__(self):
    self.old_page = self.browser.find_element_by_tag_name('html')
    
  def page_has_loaded(self):
    new_page = self.browser.find_element_by_tag_name('html')
    return new_page.id != self.old_page.id
    
  def __exit__(self, *_):
    wait_for(self.page_has_loaded)

class Control:
    def __init__(self, remote = False, driver = None):
        self.token = None

        if driver is not None:
            self.driver = driver
        else:
            if remote:
                self.driver = webdriver.Remote(command_executor = Config.server, desired_capabilities=DesiredCapabilities.FIREFOX)
            else:
                self.driver = webdriver.Firefox()

    def go_to(self, address):
        self.driver.get(Config.address + address)
        
    
    def go_to_admin(self, address):
        address = "admin/" + address + "&token=" + self.token
        self.go_to(address)

    def admin_login(self):
        if self.token is not None:
            return

        self.go_to("admin")
        username = self.driver.find_element_by_id("input-username")
        username.clear()
        username.send_keys(Config.username)
        password = self.driver.find_element_by_id("input-password")
        password.clear()
        password.send_keys(Config.password)
        
        with wait_for_page_load(self.driver):
            password.send_keys(Keys.ENTER)
        
        self.token = re.search('token=(.*)$', self.driver.current_url).group(1)

    def get_driver(self):
        return self.driver