from .control import *;

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class CartControl (Control):
    def add_product(self, id, amount = 1):
        self.go_to("index.php?route=product/product&product_id=" + str(id))

        input = self.driver.find_element_by_id("input-quantity")
        input.clear()
        input.send_keys(str(amount))

        button = self.driver.find_element_by_id("button-cart")
        button.click()

    def cart_amount(self, id = None):
        self.go_to("index.php?route=checkout/cart")
        
        amount = 0
        if id is not None:
            try:
                link = self.driver.find_element_by_css_selector("#content a[href$='product_id="+str(id)+"']")
            except Exception:
                return amount

            input = link.find_element_by_xpath("../..").find_element_by_css_selector("input[name^='quantity']")
            amount = input.get_attribute("value")
        else:
            try:
                inputs = self.driver.find_elements_by_css_selector("#content input[name^='quantity']")
                for i in inputs:
                    amount = amount + int(i.get_attribute("value"))
            except Exception:
                amount = 0

        return amount

    def change_amount(self, id, amount):
        self.go_to("index.php?route=checkout/cart")
        link = self.driver.find_element_by_css_selector("#content a[href$='product_id="+str(id)+"']")
        input = link.find_element_by_xpath("../..").find_element_by_css_selector("input[name^='quantity']")
        newAmount = int(input.get_attribute("value")) + amount
        input.clear()
        input.send_keys(str(newAmount))
        input.send_keys(Keys.ENTER)

    def remove_product(self, id):
        self.go_to("index.php?route=checkout/cart")
        link = self.driver.find_element_by_css_selector("#content a[href$='product_id="+str(id)+"']")
        button = link.find_element_by_xpath("../..").find_element_by_css_selector("button[data-original-title='Remove']")
        button.click()

    def use_coupon(self, coupon):
        self.go_to("index.php?route=checkout/cart")
        self.driver.find_element_by_css_selector("a[href='#collapse-coupon']").click()
        input = self.driver.find_element_by_id("input-coupon")
        input.clear()
        input.send_keys(str(coupon))

        button = self.driver.find_element_by_id("button-coupon")
        button.click()

    def use_voucher(self, voucher):
        self.go_to("index.php?route=checkout/cart")
        self.driver.find_element_by_css_selector("a[href='#collapse-voucher']").click()
        input = self.driver.find_element_by_id("input-voucher")
        input.clear()
        input.send_keys(str(voucher))

        button = self.driver.find_element_by_id("button-voucher")
        button.click()

    def disable_product(self, id):
        self.admin_login()
        self.go_to_admin("index.php?route=catalog/product/edit&product_id=" + str(id))
        self.driver.find_element_by_css_selector("a[href='#tab-data']").click()
        self.driver.find_element_by_id("input-status").find_element_by_css_selector("option[value='0']").click()

        button = self.driver.find_element_by_css_selector("button[data-original-title='Save']")
        button.click()

    def enable_product(self, id):
        self.admin_login()
        self.go_to_admin("index.php?route=catalog/product/edit&product_id=" + str(id))
        self.driver.find_element_by_css_selector("a[href='#tab-data']").click()
        self.driver.find_element_by_id("input-status").find_element_by_css_selector("option[value='1']").click()
        
        button = self.driver.find_element_by_css_selector("button[data-original-title='Save']")
        button.click()

    def get_total_value(self):
        self.go_to("index.php?route=checkout/cart")
        td = self.driver.find_element_by_id("content").find_element_by_xpath("//*[text() = 'Total:']").find_element_by_xpath("../..").find_element_by_css_selector("td:last-of-type")
        return td.get_attribute('innerHTML')
