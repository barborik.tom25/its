from behave import *
from control.cart import *
from control.checkout1 import *

cc = None
ce = None

@given("zakaznik ma v kosiku {id}")
def step_impl(context, id):
    global cc
    global ce
    cc = CartControl()
    ce = Checkout1Control(driver = cc.get_driver())
    cc.add_product(int(id), 1)
    pass

@when("zakaznik se pokusi prihlasit s {name} {password}")
def step_impl(context, name, password):
    global ce
    ce.user_login(name, password)
    pass

@then("zakaznik byl {state}")
def step_impl(context, state):
    global ce
    if state == "prihlasen":
        assert ce.user_logged() == True
    else:
        assert ce.user_logged() != True
    ce.get_driver().close()
    pass