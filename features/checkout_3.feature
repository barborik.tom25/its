Feature: Zákazník už se dostal přes 2. krok, kde vyplnil, popřípadě vybral,
	doručovací adresu, na třetí krok, kde si vybírá platební metodu
	a po úplném a důkladném přečtení podmínek, souhlasí s nimi.
	
	Scenario: Zákazníkovi systém vypíše alespoň jednu platební metodu.
		Given zakaznik se nachazi na vyberu platby
		Then zakaznik ma alespon 1 moznost platby
		
	Scenario: Zákazník odsouhlasil podmínky obchodu, ale zapomněl si vybrat,
		jakou platební metodu použije.
		Given zakaznik se nachazi na vyberu platby
		and zakaznik odsouhlasil podminky
		When zakaznik nevybere zadnou platbu
		Then zakaznik je upozornen na platbu
	
	Scenario: Zákazník si vybral, jakým způsobem chce platit, ale zapomněl
		před pokračováním souhlasit s podmínkami obchodu.
		Given zakaznik se nachazi na vyberu platby
		and zakaznik vybral platbu
		When zakaznik neodsouhlasil podminky
		Then zakaznik je upozornen na podminky
		
	Scenario: Zákazník omylem kliknul na pokračování na další krok, čímž
		nevybral ani platební metodu, ani neodsouhlasil podmínky.
		Given zakaznik se nachazi na vyberu platby
		and zakaznik nevybral platbu
		When zakaznik neodsouhlasil podminky
		Then zakaznik je upozornen na platbu
		and zakaznik je upozornen na podminky
		
	Scenario: Zákazník si vybral, jakým způsobem bude za nákup platit a
		odsouhlasil podmínky obchodu. Teď chce pokračovat na poslední krok.
		Given zakaznik se nachazi na vyberu platby
		and zakaznik vybral platbu
		and zakaznik odsouhlasil podminky
		When zakaznik chce pokracovat na dalsi krok
		Then zakaznik je pusten na potvrzeni objednavky
