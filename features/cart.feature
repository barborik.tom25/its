Feature: Zákazník přejde na stránku košíku, aby ještě než bude objednávat,
	překontroloval jeho obsah, a pokud mu nebude něco seděl, ještě ho
	upravil.

	Scenario: Zákazník má v košíku položku, kterou do něj vložil omylem.
		(odebrani)
		Given administrator povoli vyrobek (43)
		And v kosiku se nachazi 1 - 43
		When uzivatel odebere polozku 43
		Then kosik je prazdny

	Scenario: Zákaznik si vzpomněl, za potřebuje ještě jednu položku. 
		(změna počtu + 1)
		Given v kosiku se nachazi 1 - 43
		When uzivatel zmeni mnozstvi 43 o +1
		Then v kosiku se nachazi 2 polozka

	Scenario: Zákazník se při vkládání překlikl a omylem vložil o položku navíc.
		(změna počtu o -1)
		Given v kosiku se nachazi 2 - 43
		When uzivatel zmeni mnozstvi 43 o -1
		Then v kosiku se nachazi 1 polozka
	
	Scenario: Zákazník přidal položku k již existující navíc do košíku a teď
		očekává, že se změní i celková cena obsahu košíku.
		Given v kosiku se nachazi 1 - 43
		When uzivatel zmeni mnozstvi 43 o +1
		Then celkova cena je $1204.00

	Scenario: Zákazník už zde jednou nakoupil a dostal pro příští nákup
		kupo, tak by ho chtěl teď využít. (kupon)
		Given v kosiku se nachazi 1 - 43
		When uzivatel pouzije kupon 2222
		Then celkova cena je $542.00

	Scenario: Zákazník dostal od rodiny dárkový poukaz na nákup, tak
		by s ním teď chtěl nakoupit zboží. (dárkový voucher)
		Given v kosiku se nachazi 1 - 43
		When uzivatel pouzije voucher v001
		Then celkova cena je $502.00

	Scenario: Zákazník si do košíku vložil zboží, ale zatímco ho kontroluje,
		obchod přestal dané zboží nabízet. (změna stavu => disabled)
		Given v kosiku se nachazi 1 - 43
		When administrator zakaze vyrobek (43)
		Then kosik je prazdny
